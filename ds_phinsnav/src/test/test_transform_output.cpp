/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/2/20.
//

#include <gtest/gtest.h>

#include <ds_phinsnav/phinsnav.h>

class PhinsNavTransform : public ::testing::Test {
 protected:
  void SetUp() override {

    test_msg.header.stamp = ros::Time(12345, 678e6);
    // point straight forward
    test_msg.attitude_quaternion[0] = 1.0;
    test_msg.attitude_quaternion[1] = 0.0;
    test_msg.attitude_quaternion[2] = 0.0;
    test_msg.attitude_quaternion[3] = 0.0;

    test_msg.longitude = 0;
    test_msg.latitude = 0;
    test_msg.altitude = 0;
    test_msg.body_rates_XVn[0] = 0.1;
    test_msg.body_rates_XVn[1] = 0.2;
    test_msg.body_rates_XVn[2] = 0.3;

    test_msg.body_velocity_XVn[0] = 0.4;
    test_msg.body_velocity_XVn[1] = 0.5;
    test_msg.body_velocity_XVn[2] = 0.6;

    proj.init(0, 0);
  }

  ds_sensor_msgs::PhinsStdbin3 test_msg;
  phinsnav::Projection proj;
};

TEST_F(PhinsNavTransform, identity) {

  phinsnav::NavSolution result = phinsnav::PhinsNav::transformToSolution(test_msg, Eigen::Affine3d::Identity(), proj);

  EXPECT_NEAR(12345.678, result.stamp.toSec(), 1.0e-3);
  EXPECT_NEAR(0, result.lon, 1.0e-9);
  EXPECT_NEAR(0, result.lat, 1.0e-9);
  EXPECT_NEAR(0, result.pos_enu(0), 1.0e-4);
  EXPECT_NEAR(0, result.pos_enu(1), 1.0e-4);
  EXPECT_NEAR(0, result.pos_enu(2), 1.0e-4);

  // NOT the identity because this transforms the goofy PHINS frame to standard ENU
  EXPECT_NEAR(1.0/sqrt(2.0), result.orientation.w(), 1.0e-7);
  EXPECT_NEAR(0.0, result.orientation.x(), 1.0e-7);
  EXPECT_NEAR(0.0, result.orientation.y(), 1.0e-7);
  EXPECT_NEAR(1.0/sqrt(2.0), result.orientation.z(), 1.0e-7);


}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}