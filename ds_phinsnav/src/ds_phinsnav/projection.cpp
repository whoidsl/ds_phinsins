/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/13/20.
//

#include <GeographicLib/Constants.hpp>
#include <ros/console.h>
#include "ds_phinsnav/projection.h"

namespace phinsnav {

Projection::Projection() : proj(GeographicLib::Constants::WGS84_a(),
                                GeographicLib::Constants::WGS84_f(), 1.0) {
  origin = Eigen::Vector2d::Zero();
  false_en = Eigen::Vector2d::Zero();
  valid = false;
}

void Projection::init(double lon, double lat) {
  // forward-project our origin into the projection to subtract it out later
  ROS_INFO_STREAM("Setting origin to " <<lon <<"/" <<lat);
  origin(0) = lon;
  origin(1) = lat;
  proj.Forward(origin(0), origin(1), origin(0), false_en(0), false_en(1));
  valid = true;
}

// lon, THEN lat
Eigen::Vector2d Projection::ll2proj(const Eigen::Vector2d &ll) const {
  Eigen::Vector2d ret_en;
  proj.Forward(origin(0), ll(1), ll(0), ret_en(0), ret_en(1));
  return ret_en - false_en;
}

// lon, THEN lat
Eigen::Vector2d Projection::proj2ll(const Eigen::Vector2d &en) const {
  Eigen::Vector2d ret_ll;
  Eigen::Vector2d corr = en + false_en;
  proj.Reverse(origin(0), corr(0), corr(1), ret_ll(0), ret_ll(1));
  return ret_ll;
}

} // namespace phinsnav
