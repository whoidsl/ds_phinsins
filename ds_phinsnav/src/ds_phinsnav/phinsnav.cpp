/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by vast on 12/2/20.
//

#include "ds_phinsnav/phinsnav.h"

#include <geometry_msgs/PointStamped.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_listener.h>
#include <Eigen/Geometry>

namespace phinsnav {

PhinsNav::PhinsNav() : ds_base::DsProcess() {
  frame_init = false;
}

PhinsNav::PhinsNav(int argc, char *argv[], const std::string &name) : ds_base::DsProcess(argc, argv, name) {
  frame_init = false;
}

PhinsNav::~PhinsNav() = default;

void PhinsNav::setupParameters() {
  ds_base::DsProcess::setupParameters();

  // Load our TF frame names
  if (!ros::param::get("~body_link", vehicle_link)) {
    ROS_FATAL("MUST set body_link for vehicle!");
    ROS_FATAL("Got: %s", vehicle_link.c_str());
    ROS_BREAK();
  }

  if (!ros::param::get("~con_link", control_point_link)) {
    ROS_FATAL("MUST set con_link for vehicle!");
    ROS_FATAL("Got: %s", control_point_link.c_str());
    ROS_BREAK();
  }

  if (!ros::param::get("~map_link", map_link)) {
    ROS_FATAL("MUST set map_link for filter!");
    ROS_BREAK();
  }

  // setup the parameter downsample options
  publisher_downsample = 10;
  publisher_skipped = 0;
  ros::param::get("~/downsample", publisher_downsample);

  // initialize the projection, if appropriate
  double origin_lat = 0, origin_lon = 0;
  if (ros::param::get(ros::this_node::getNamespace() + "/origin/latitude", origin_lat)
      && ros::param::get(ros::this_node::getNamespace() + "/origin/longitude", origin_lon)) {

    ROS_INFO("Loading nav lat/lon origin as %.9f/%.9f", origin_lat , origin_lon);
    local_proj.init(origin_lon, origin_lat);
  } else {
    ROS_INFO_STREAM("No nav origin on parameter server; waiting for first INS fix");
  }

  frame_init = false;
}

void PhinsNav::setupTransforms(const ds_sensor_msgs::PhinsStdbin3 &msg) {
  filter_link = msg.header.frame_id;

  // load the relevant transforms
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);
  try {
    geometry_msgs::TransformStamped raw_tf = tfBuffer.lookupTransform(vehicle_link,
                                                                      filter_link, ros::Time(0), ros::Duration(10.0));
    ins2body_ = tf2::transformToEigen(raw_tf);

    ROS_INFO_STREAM("Got body link in filter frame: " << ins2body_.translation().transpose());

  } catch (tf2::TransformException &ex) {
    ROS_WARN("Unable to lookup transform between %s and %s: %s",
             vehicle_link.c_str(), filter_link.c_str(), ex.what());
    // if we can't get the transform, skip and hope it becomes available
    ROS_BREAK();
  }

  try {
    geometry_msgs::TransformStamped raw_tf = tfBuffer.lookupTransform(control_point_link,
                                                                      filter_link, ros::Time(0), ros::Duration(10.0));
    ins2control_ = tf2::transformToEigen(raw_tf);

    ROS_INFO_STREAM("Got controller link in filter frame: " << ins2control_.translation().transpose());

  } catch (tf2::TransformException &ex) {
    ROS_WARN("Unable to lookup transform between %s and %s: %s",
             control_point_link.c_str(), filter_link.c_str(), ex.what());
    // if we can't get the transform, skip and hope it becomes available
    ROS_BREAK();
  }
}

void PhinsNav::setupPublishers() {
  ds_base::DsProcess::setupPublishers();
  auto nh = nodeHandle();

  pub_navagg = nh.advertise<ds_nav_msgs::AggregatedState>(ros::this_node::getName() + "/navagg", 10);
  pub_navstate = nh.advertise<ds_nav_msgs::NavState>(ros::this_node::getName() + "/navstate", 10);
  pub_navsat = nh.advertise<sensor_msgs::NavSatFix>(ros::this_node::getName() + "/navsat", 10);
  pub_ext_fix = nh.advertise<geometry_msgs::PointStamped>(ros::this_node::getName() + "/globalfix", 10);
  pub_pose = nh.advertise<geometry_msgs::PoseStamped>(ros::this_node::getName() + "/pose", 10);
  pub_ctrl_pt = nh.advertise<geometry_msgs::PoseStamped>(ros::this_node::getName() + "/control_pt", 10);
}
void PhinsNav::setupSubscriptions() {

  ds_base::DsProcess::setupPublishers();
  auto nh = nodeHandle();

  sub_phins = nh.subscribe<ds_sensor_msgs::PhinsStdbin3>("phinsbin", 10, &PhinsNav::phinsbin_callback, this);
  sub_ext_global = nh.subscribe<sensor_msgs::NavSatFix>("ext_fix", 10, &PhinsNav::navsat_callback, this);
}

void PhinsNav::phinsbin_callback(const boost::shared_ptr<const ds_sensor_msgs::PhinsStdbin3> &msg) {
  // lookup our transforms, if required
  if (!frame_init) {
    setupTransforms(*msg);
    frame_init = true;
  }

  // convert to xyz
  if (!local_proj.valid) {
    ROS_INFO("Initializing PHINS BINARY intepreter to origin %.7f x %.7f", msg->longitude, msg->latitude);
    local_proj.init(msg->longitude, msg->latitude);
  }

  // send the TF update
  NavSolution body_solution = transformToSolution(*msg, ins2body_, local_proj);
  tf2_broadcaster.sendTransform(msgToTransform(body_solution));

  // all other updates happen at the slower rate
  if (publisher_skipped >= publisher_downsample - 1) {
    publisher_skipped = 0;

    NavSolution control_solution = transformToSolution(*msg, ins2control_, local_proj);

    pub_navagg.publish(msgToAggState(control_solution));
    pub_navstate.publish(msgToNavState(control_solution));
    pub_pose.publish(msgToPose(body_solution));
    pub_ctrl_pt.publish(msgToPose(control_solution));
  } else {
    publisher_skipped++;
  }
}

geometry_msgs::TransformStamped PhinsNav::msgToTransform(const NavSolution &navsol) const {
  geometry_msgs::TransformStamped ret;

  ret.header.frame_id = map_link;
  ret.header.stamp = navsol.stamp;
  ret.child_frame_id = vehicle_link;

  ret.transform.rotation = tf2::toMsg(navsol.orientation);
  // transform is in East/North/Up
  ret.transform.translation.x = navsol.pos_enu(0);
  ret.transform.translation.y = navsol.pos_enu(1);
  ret.transform.translation.z = navsol.pos_enu(2);

  return ret;
}

const static double DTOR = M_PI / 180.0;

ds_nav_msgs::AggregatedState PhinsNav::msgToAggState(const NavSolution &navsol) const {
  ds_nav_msgs::AggregatedState ret;

  ret.header.stamp = navsol.stamp;
  ret.header.frame_id = map_link;

  // meters
  ret.northing.value = navsol.pos_enu(1);
  ret.northing.valid = navsol.valid_lonlat;
  ret.easting.value = navsol.pos_enu(0);
  ret.easting.valid = navsol.valid_lonlat;
  ret.down.value = -navsol.pos_enu(2);
  ret.down.valid = navsol.valid_depth;

  // radians
  ret.roll.value = DTOR * navsol.roll_deg;
  ret.roll.valid = navsol.valid_attitude;
  ret.pitch.value = DTOR * navsol.pitch_deg;
  ret.pitch.valid = navsol.valid_attitude;
  ret.heading.value = DTOR * navsol.hdg_deg;
  ret.heading.valid = navsol.valid_attitude;

  // meters / second, fwd/stbd/down
  ret.surge_u.value = navsol.vel_fpu(0);
  ret.surge_u.valid = navsol.valid_velocity;
  ret.sway_v.value = -navsol.vel_fpu(1);
  ret.sway_v.valid = navsol.valid_velocity;
  ret.heave_w.value = -navsol.vel_fpu(2);
  ret.heave_w.valid = navsol.valid_velocity;

  // angular rates-- convert to fwd/stbd/down
  ret.p.value = DTOR * navsol.angvel_deg_fpu(0);
  ret.p.valid = navsol.valid_rates;
  ret.q.value = -DTOR * navsol.angvel_deg_fpu(1);
  ret.q.valid = navsol.valid_rates;
  ret.r.value = -DTOR * navsol.angvel_deg_fpu(2);
  ret.r.valid = navsol.valid_rates;

  // accelerations
  // TODO

  return ret;
}

ds_nav_msgs::NavState PhinsNav::msgToNavState(const NavSolution &navsol) const {
  ds_nav_msgs::NavState ret;

  ret.header.stamp = navsol.stamp;
  ret.header.frame_id = map_link;

  // degrees
  if (navsol.valid_lonlat) {
    ret.lat = navsol.lat;
    ret.lon = navsol.lon;
  } else {
    ret.lat = std::numeric_limits<double>::quiet_NaN();
    ret.lon = std::numeric_limits<double>::quiet_NaN();
  }

  // meters
  if (navsol.valid_lonlat) {
    ret.northing = navsol.pos_enu(1);
    ret.easting = navsol.pos_enu(0);
  } else {
    ret.northing = std::numeric_limits<double>::quiet_NaN();
    ret.easting = std::numeric_limits<double>::quiet_NaN();
  }
  if (navsol.valid_depth) {
    ret.down = -navsol.pos_enu(2);
  } else {
    ret.down = std::numeric_limits<double>::quiet_NaN();
  }

  // degrees
  if (navsol.valid_attitude) {
    ret.roll = navsol.roll_deg;
    ret.pitch = navsol.pitch_deg;
    ret.heading = navsol.hdg_deg;
  } else {
    ret.roll = std::numeric_limits<double>::quiet_NaN();
    ret.pitch = std::numeric_limits<double>::quiet_NaN();
    ret.heading = std::numeric_limits<double>::quiet_NaN();
  }

  // m/s-- fwd/stbd/down, while phinsbin is fwd/port/up
  if (navsol.valid_velocity) {
    ret.surge_u = navsol.vel_fpu(0);
    ret.sway_v = -navsol.vel_fpu(1);
    ret.heave_w = -navsol.vel_fpu(2);
  } else {
    ret.surge_u = navsol.vel_fpu(0);
    ret.sway_v = -navsol.vel_fpu(1);
    ret.heave_w = -navsol.vel_fpu(2);
  }

  // rad/s-- again, there's a fwd/stbd/down vs. fwd/port/up conversion
  if (navsol.valid_rates) {
    ret.p = DTOR * navsol.angvel_deg_fpu(0);
    ret.q = -DTOR * navsol.angvel_deg_fpu(1);
    ret.r = -DTOR * navsol.angvel_deg_fpu(2);
  } else {
    ret.p = DTOR * navsol.angvel_deg_fpu(0);
    ret.q = -DTOR * navsol.angvel_deg_fpu(1);
    ret.r = -DTOR * navsol.angvel_deg_fpu(2);
  }

  return ret;
}

sensor_msgs::NavSatFix PhinsNav::msgToNavSat(const NavSolution &navsol) const {
  sensor_msgs::NavSatFix ret;

  ret.header.stamp = navsol.stamp;
  ret.header.frame_id = vehicle_link;

  ret.latitude = navsol.lat;
  ret.longitude = navsol.lon;
  ret.altitude = navsol.pos_enu(2);
  ret.status.status = sensor_msgs::NavSatStatus::STATUS_FIX;
  ret.status.service = sensor_msgs::NavSatStatus::SERVICE_COMPASS;

  // ignore navsol validity flags

  // NavSatFix covariance is ENU, so we convert from NED on the fly
  ret.position_covariance_type = sensor_msgs::NavSatFix::COVARIANCE_TYPE_KNOWN;
  ret.position_covariance[0] = navsol.pos_covar(0, 0);
  ret.position_covariance[1] = navsol.pos_covar(0, 1);
  ret.position_covariance[2] = navsol.pos_covar(0, 2);
  ret.position_covariance[3] = navsol.pos_covar(1, 0);
  ret.position_covariance[4] = navsol.pos_covar(1, 1);
  ret.position_covariance[5] = navsol.pos_covar(1, 2);
  ret.position_covariance[6] = navsol.pos_covar(2, 0);
  ret.position_covariance[7] = navsol.pos_covar(2, 1);
  ret.position_covariance[8] = navsol.pos_covar(2, 2);

  return ret;
}

geometry_msgs::PoseStamped PhinsNav::msgToPose(const NavSolution &navsol) const {
  geometry_msgs::PoseStamped ret;

  ret.header.stamp = navsol.stamp;
  ret.header.frame_id = map_link;

  ret.pose.orientation = tf2::toMsg(navsol.orientation);
  // as usual, ROS is in ENU.  Convert on the fly
  ret.pose.position.x = navsol.pos_enu(0);
  ret.pose.position.y = navsol.pos_enu(1);
  ret.pose.position.z = navsol.pos_enu(2);
  // ignore navsol validity flags

  return ret;
}

void PhinsNav::init_proj(double lon, double lat) {
  local_proj.init(lon, lat);
}

void quat2eul(double& roll, double& pitch, double& heading, const Eigen::Quaterniond& orientation) {
  // First, we have to convert from ENU/FPU in the navigation frame to NED/FSD in order to
  // get the correct Euler angles out.  We'll use rotation matrices because rotation matrix
  // math is slightly faster for long transform chains.
  Eigen::Matrix3d Renu2ned; // swap x/y, flip sign on z
  Renu2ned <<0, 1, 0,
             1, 0, 0,
             0, 0, -1;
  // NOTE: Quaternion would be Eigen::Quaterniond enu2ned(0, M_SQRT1_2, M_SQRT2/2.0, 0);

  Eigen::Matrix3d Rfpu2fsd;  // flip sign on y and z
  Rfpu2fsd <<1, 0, 0,
             0, -1, 0,
             0, 0, -1;

  Eigen::Quaterniond q(Renu2ned * orientation.toRotationMatrix() * Rfpu2fsd);

  // ok, so teh Euler angle conversion in Eigen is screwed up.  We'll instead use the formulate directly from
  // page 168 of Kuipers.  His notation is slightly different than Eigen's, with q0 -> w and q1-3 -> x-z
  double m11 = 2.0 * q.w() * q.w() + 2.0 * q.x() * q.x() - 1.0;
  double m12 = 2.0 * q.x() * q.y() + 2.0 * q.w() * q.z();
  double m13 = 2.0 * q.x() * q.z() - 2.0 * q.w() * q.y();
  double m23 = 2.0 * q.y() * q.z() + 2.0 * q.w() * q.x();
  double m33 = 2.0 * q.w() * q.w() + 2.0 * q.z() * q.z() - 1.0;

  roll = atan2(m23, m33) * 180.0 / M_PI;
  pitch = asin(-m13) * 180.0 / M_PI;
  heading = atan2(m12, m11) * 180.0 / M_PI;
  // atan2 will output +/- pi, but we want [0, 360)
  if (heading < 0) {
    heading += 360.0;
  }
}

NavSolution PhinsNav::transformToSolution(const ds_sensor_msgs::PhinsStdbin3 &msg,
                                          const Eigen::Affine3d &offset,
                                          const Projection& proj,
                                          const Eigen::Vector3d& enu_delta ) {
  NavSolution ret;

  Eigen::Vector3d offset_fpu(offset.translation());
  Eigen::Vector3d offset_fsd(offset_fpu(0), -offset_fpu(1), -offset_fpu(2));
  // Time, the easy one
  ret.stamp = msg.header.stamp;

  // convert the PHINS NWU-defined quaternion to proper ENU
  static const Eigen::Matrix3d nwu2enu(
      Eigen::Quaterniond(sqrt(2.0)/2.0, 0, 0, sqrt(2.0)/2.0).toRotationMatrix());

  Eigen::Matrix3d Renu2ned;
  Renu2ned <<0, 1, 0,
             1, 0, 0,
             0, 0, -1;

  Eigen::Quaterniond raw_phins_quat;
  raw_phins_quat.w() = msg.attitude_quaternion[0];
  raw_phins_quat.x() = msg.attitude_quaternion[1];
  raw_phins_quat.y() = msg.attitude_quaternion[2];
  raw_phins_quat.z() = msg.attitude_quaternion[3];
  // composing quaternions is slower than multiplying 3x3 matrices, so do that instead
  Eigen::Matrix3d Rphins2global = nwu2enu * (raw_phins_quat.inverse().toRotationMatrix());
  Eigen::Matrix3d Rframe2global_ned = Rphins2global * offset.rotation().transpose();
  ret.orientation = Eigen::Quaterniond(Rframe2global_ned);

  // fill in our euler angles
  quat2eul(ret.roll_deg, ret.pitch_deg, ret.hdg_deg, Eigen::Quaterniond(Rframe2global_ned * offset.rotation().transpose()));

  // project to the local coordinate frame.  Apply a leverarm.  Project back
  auto en = proj.ll2proj(Eigen::Vector2d(msg.longitude, msg.latitude));
  Eigen::Vector3d enu_filt(en(0), en(1), msg.altitude);
  ret.pos_enu = enu_filt + enu_delta - Rphins2global * offset.translation();
  auto ll = proj.proj2ll(Eigen::Vector2d(ret.pos_enu(0), ret.pos_enu(1)));
  ret.lon = ll(1);
  ret.lat = ll(0);

  // compute the rotation rate
  Eigen::Vector3d phins_rates(msg.body_rates_XVn[0], msg.body_rates_XVn[1], msg.body_rates_XVn[2]);
  // phins rates are in the PHINS frame, transform to the desired frame
  ret.angvel_deg_fpu = offset.rotation() * phins_rates;

  //ROS_ERROR_STREAM("offset: " <<offset.translation().transpose());
  Eigen::Vector3d corr = DTOR*ret.angvel_deg_fpu.cross(offset.translation());

  // compute the velocity update.
  Eigen::Vector3d phins_vel(msg.body_velocity_XVn[0], msg.body_velocity_XVn[1], msg.body_velocity_XVn[2]);

  //ret.vel_fpu = (phins_vel - DTOR*ret.angvel_deg_fpu.cross(offset.translation()));
  ret.vel_fpu = (phins_vel + corr);

  // fill in covariance
  Eigen::Matrix3d raw_covar = Eigen::Matrix3d::Zero();
  // PHINS covar
  raw_covar(0,0) = msg.position_cov[3];
  raw_covar(0,1) = msg.position_cov[1];
  raw_covar(1,0) = msg.position_cov[2];
  raw_covar(1,1) = msg.position_cov[0];
  raw_covar(2,2) = msg.altitude_stddev*msg.altitude_stddev;

  ret.pos_covar = offset.rotation() * raw_covar * offset.rotation().transpose();

  // flip E/N and invert the sign of U to
  ret.vel_enu(0) = msg.velocity_NEU[1];
  ret.vel_enu(1) = msg.velocity_NEU[0];
  ret.vel_enu(2) = msg.velocity_NEU[2];

  ret.vel_enu_covar = Eigen::Matrix3d::Zero();
  ret.vel_enu_covar(0,0) = msg.velocity_stddev_NEU[1]*msg.velocity_stddev_NEU[1];
  ret.vel_enu_covar(1,1) = msg.velocity_stddev_NEU[0]*msg.velocity_stddev_NEU[0];
  ret.vel_enu_covar(2,2) = msg.velocity_stddev_NEU[2]*msg.velocity_stddev_NEU[2];

  ret.valid_lonlat = true;
  ret.valid_velocity = true;
  ret.valid_depth = true;
  ret.valid_attitude = true;

  return ret;
}

void PhinsNav::navsat_callback(const boost::shared_ptr<const sensor_msgs::NavSatFix> &msg) {
  if (local_proj.valid) {
    geometry_msgs::PointStamped ret;
    ret.header.stamp = msg->header.stamp;
    ret.header.frame_id = map_link;

    auto en = local_proj.ll2proj(Eigen::Vector2d(msg->longitude, msg->latitude));
    ret.point.x = en(0);
    ret.point.y = en(1);
    //ret.point.z = msg->altitude;
    ret.point.z = 0; // Just pin to the surface; altitude is NEVER right (b/c ellipsoid, not geoid)

    pub_ext_fix.publish(ret);
  }
}
} // namespace phinsnav
