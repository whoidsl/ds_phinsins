/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by vast on 12/2/20.
//

#ifndef PHINSNAV_PHINSNAV_H_
#define PHINSNAV_PHINSNAV_H_

#include <ds_base/ds_process.h>

#include <tf2_ros/transform_broadcaster.h>
#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <ds_nav_msgs/AggregatedState.h>
#include <ds_nav_msgs/NavState.h>
#include <sensor_msgs/NavSatFix.h>

#include "projection.h"
#include <Eigen/Dense>

namespace phinsnav {

struct NavSolution {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  NavSolution() {
    lat = 0;
    lon = 0;
    roll_deg = 0;
    pitch_deg = 0;
    hdg_deg = 0;

    valid_lonlat = false;
    valid_velocity = false;
    valid_depth = false;
    valid_attitude = false;
    valid_rates = false;
  }

  ros::Time stamp;

  double lat;
  double lon;

  Eigen::Vector3d pos_enu;
  Eigen::Matrix3d pos_covar;
  Eigen::Vector3d vel_fpu;

  // global-frame velocities
  Eigen::Vector3d vel_enu;
  // velocity covariance, in ENU (as the PHINS uses a filter with global velocity estimates, NOT body!)
  Eigen::Matrix3d vel_enu_covar;

  Eigen::Quaterniond orientation;
  double roll_deg;
  double pitch_deg;
  double hdg_deg;

  bool valid_lonlat;
  bool valid_velocity;
  bool valid_depth;
  bool valid_attitude;
  bool valid_rates;

  Eigen::Vector3d angvel_deg_fpu;
};

class PhinsNav : public ds_base::DsProcess {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  explicit PhinsNav();
  PhinsNav(int argc, char* argv[], const std::string& name);
  ~PhinsNav() override;
  DS_DISABLE_COPY(PhinsNav);

  void init_proj(double lon, double lat);

  /// Transform the phins message to
  /// \param msg Raw PHINS message
  /// \param offset Vehicle-frame offset to apply to the solution
  /// \param proj Lat/Lon to local projection
  /// \param enu_delta East/North/Up shift to apply to the PHINS position, typically out of a backseat filter
  /// \return The
  static NavSolution transformToSolution(const ds_sensor_msgs::PhinsStdbin3& msg, const Eigen::Affine3d& offset,
                                         const Projection& proj, const Eigen::Vector3d& enu_delta = Eigen::Vector3d::Zero());

 protected:
  void setupParameters() override;
  void setupPublishers() override;
  void setupSubscriptions() override;

  ros::Subscriber sub_phins;
  ros::Subscriber sub_ext_global;

  ros::Publisher pub_navagg;
  ros::Publisher pub_navstate;
  ros::Publisher pub_navsat;
  ros::Publisher pub_ext_fix;
  ros::Publisher pub_pose;
  ros::Publisher pub_ctrl_pt;

  // TF output
  tf2_ros::TransformBroadcaster tf2_broadcaster;

  int publisher_skipped;
  int publisher_downsample;

  Projection local_proj;
  std::string filter_link;
  std::string vehicle_link;
  std::string control_point_link;
  std::string map_link;

  bool frame_init;
  Eigen::Affine3d ins2body_;
  Eigen::Affine3d ins2control_;

  void phinsbin_callback(const boost::shared_ptr<const ds_sensor_msgs::PhinsStdbin3>& msg);
  void navsat_callback(const boost::shared_ptr<const sensor_msgs::NavSatFix>& msg);

  geometry_msgs::TransformStamped msgToTransform(const NavSolution& navsol) const;
  ds_nav_msgs::AggregatedState msgToAggState(const NavSolution& navsol) const;
  ds_nav_msgs::NavState msgToNavState(const NavSolution& navsol) const;
  sensor_msgs::NavSatFix msgToNavSat(const NavSolution& navsol) const;
  geometry_msgs::PoseStamped msgToPose(const NavSolution& navsol) const;

  void setupTransforms(const ds_sensor_msgs::PhinsStdbin3& msg);
};

} // namespace phinsnav

#endif // PHINSNAV_PHINSNAV_H_
