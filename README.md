# Overview

This repository contains packages for running the PHINS as an aided INS using standard ds_sensor_msgs messages.  The repositories are:

* ds_phinsins The repository contains the actual driver.  It extends the ds_sensors phinsbin driver (in the C++ class-inheritence sense) by adding stuff to aide and/or control the PHINS.
* ds_phinsnav This repostiory contains a node that provides various services to get useful navigation messages for feedback controllers, TF, etc from the phinsbin message.  It is intended primarily for use with the ds_control family of packages.