/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/1/20.
//

#include <ds_phinsins/phinsins.h>
#include <ds_phinsins/phinstdv3.h>
#include <ds_core_msgs/VoidCmd.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <ds_nmea_parsers/util.h>

uint32_t rostime2phinstime(const ros::Time& time) {
  auto tod = time.toBoost().time_of_day();
  return static_cast<uint32_t>(tod.total_microseconds()/100.0);
}

uint32_t calc_checksum(uint8_t* buf, size_t chksumlen) {
  uint32_t ret = 0;
  for (size_t i=0; i<chksumlen; i++) {
    ret += *buf++;
  }
  return ret;
}

int32_t degToLong(double deg) {
  return static_cast<int32_t>((deg / M_PI) * static_cast<double>(1 < 31));
}

uint32_t hfloat_to_be32(float in) {
  uint32_t tmp = *reinterpret_cast<uint32_t*>(&in);
  return htobe32(tmp);
}

uint64_t hdouble_to_be64(double in) {
  uint64_t tmp = *reinterpret_cast<uint64_t*>(&in);
  return htobe64(tmp);
}

namespace ds_sensors {

PhinsIns::PhinsIns() : ds_sensors::PhinsBin(), query_idx_(0), send_on_next_config_(true),
                       restart_in_progress_(false), sv_min(1400), sv_max(1600) {
}

PhinsIns::PhinsIns(int argc, char* argv[], const std::string& name)
    : ds_sensors::PhinsBin(argc, argv, name), query_idx_(0), send_on_next_config_(true),
      restart_in_progress_(false), sv_min(1400), sv_max(1600) {
}

PhinsIns::~PhinsIns() = default;
void PhinsIns::setupParameters() {
  ds_sensors::PhinsBin::setupParameters();

  // pick the filter modes
  const static int DISABLE = 1; // always reject
  const static int ENABLE = 2; // normal accept

  gps1_params_.gps_id = _phinsstdbin3::gps_id::gps1;
  gps1_params_.setupParameters("~/gps1");
  if (ros::param::param("~/gps1/enabled", false)) {
    initial_dynamic_config_.gps1Rejection = ENABLE;
  } else {
    initial_dynamic_config_.gps1Rejection = DISABLE;
  }

  gps2_params_.gps_id = _phinsstdbin3::gps_id::gps2;
  gps2_params_.setupParameters("~/gps2");
  if (ros::param::param("~/gps2/enabled", false)) {
    initial_dynamic_config_.gps2Rejection = ENABLE;
  } else {
    initial_dynamic_config_.gps2Rejection = DISABLE;
  }

  dvl1_params_.dvl_id = _phinsstdbin3::dvl_id::dvl1;
  dvl1_params_.setupParameters("~/dvl1");
  if (ros::param::param("~/dvl1/enabled", false)) {
    initial_dynamic_config_.dvl1Rejection = ENABLE;
  } else {
    initial_dynamic_config_.dvl1Rejection = DISABLE;
  }

  dvl2_params_.dvl_id = _phinsstdbin3::dvl_id::dvl2;
  dvl2_params_.setupParameters("~/dvl2");
  if (ros::param::param("~/dvl2/enabled", false)) {
    initial_dynamic_config_.dvl2Rejection = ENABLE;
  } else {
    initial_dynamic_config_.dvl2Rejection = DISABLE;
  }

  dvlWt1_params_.dvl_id = _phinsstdbin3::dvl_id::dvl1;
  dvlWt1_params_.setupParameters("~/dvl1_wt");
  if (ros::param::param("~/dvl1_wt/enabled", false)) {
    initial_dynamic_config_.dvlWt1Rejection = ENABLE;
  } else {
    initial_dynamic_config_.dvlWt1Rejection = DISABLE;
  }

  dvlWt2_params_.dvl_id = _phinsstdbin3::dvl_id::dvl2;
  dvlWt2_params_.setupParameters("~/dvl2_wt");
  if (ros::param::param("~/dvl2_wt/enabled", false)) {
    initial_dynamic_config_.dvlWt2Rejection = ENABLE;
  } else {
    initial_dynamic_config_.dvlWt2Rejection = DISABLE;
  }

  depth_params_.setupParameters("~/depth");
  if (ros::param::param("~/depth/enabled", false)) {
    initial_dynamic_config_.depthRejection = ENABLE;
  } else {
    initial_dynamic_config_.depthRejection = DISABLE;
  }

  usbl_params_.usbl_id = _phinsstdbin3::usbl_id::usbl1;
  usbl_params_.setupParameters("~/usbl");
  if (ros::param::param("~/usbl/enabled", false)) {
    initial_dynamic_config_.usblRejection = ENABLE;
  } else {
    initial_dynamic_config_.usblRejection = DISABLE;
  }  

  sv_min = ros::param::param("~/soundspeed_min", sv_min);
  sv_max = ros::param::param("~/soundspeed_max", sv_max);

  // Default diveMode is 2 - none or manual
  initial_dynamic_config_.diveMode = 2;
  
}
void PhinsIns::setupPublishers() {
  ds_sensors::PhinsBin::setupPublishers();

  auto nh = nodeHandle();
  
  // SS - added publishing of estimated fog and acc biases
  fogest_pub_ = nh.advertise<geometry_msgs::Vector3Stamped>(ros::this_node::getName() + "/fogest", 10);
  accest_pub_ = nh.advertise<geometry_msgs::Vector3Stamped>(ros::this_node::getName() + "/accest", 10);
}

void PhinsIns::setupServices() {
  ds_sensors::PhinsBin::setupServices();

  auto nh = nodeHandle();
  restart_srv_ = nh.advertiseService(ros::this_node::getName() + "/phins_restart", &PhinsIns::rebootCallback, this);

  // This service is intended to reinitialize the Ins with a good enough position
  // if the Ins needs to be power cycled subsea and there is a gap in usbl input
  // when the Ins is power cycled
  reinit_srv_ = nh.advertiseService(ros::this_node::getName() + "/phins_reinit", &PhinsIns::reinitCallback, this);
  
  manpos_srv_ = nh.advertiseService(ros::this_node::getName() + "/phins_manpos", &PhinsIns::manposCallback, this);

  phins_usbl_mode_srv_ = nh.advertiseService(ros::this_node::getName() + "/phins_usbl_mode", &PhinsIns::usblModeCallback, this);

}

void PhinsIns::setupSubscriptions() {
  ds_sensors::PhinsBin::setupSubscriptions();
  auto nh = nodeHandle();

  sub_gps1_ = nh.subscribe("gps1", 10, &PhinsIns::onGps1, this);
  sub_gps2_ = nh.subscribe("gps2", 10, &PhinsIns::onGps2, this);
  sub_dvl1_ = nh.subscribe("dvl1", 10, &PhinsIns::onDvl1, this);
  sub_dvl2_ = nh.subscribe("dvl2", 10, &PhinsIns::onDvl2, this);
  sub_dvlWt1_ = nh.subscribe("dvl_wt1", 10, &PhinsIns::onDvlWt1, this);
  sub_dvlWt2_ = nh.subscribe("dvl_wt2", 10, &PhinsIns::onDvlWt2, this);
  sub_depth_ = nh.subscribe("depth", 10, &PhinsIns::onDepth, this);
  sub_usbl_ = nh.subscribe("usbl", 10, &PhinsIns::onUsbl, this);
  sub_soundspeed_ = nh.subscribe("soundspeed", 10, &PhinsIns::onSoundspeed, this);
  
  sub_ext_gps_ = nh.subscribe("ext_gps" , 10, &PhinsIns::onExtGps, this);

  dr_srv_.reset(new dynamic_reconfigure::Server<ds_sensor_msgs::PhinsConfig>(dr_mutex_, this->nodeHandle("~")));
  dr_srv_->setCallback(boost::bind(&PhinsIns::dynamicParamCallback, this, _1, _2));

  // to initialize correctly, we set our current config to the default value from the server:
  dr_srv_->getConfigDefault(dynamic_config_);

  // now we trigger a callback with the initial config:
  dynamicParamCallback(initial_dynamic_config_, 0);

  // finally, we update the config on the parameter server
  dr_srv_->updateConfig(dynamic_config_);
}

void PhinsIns::setupConnections() {
  ds_sensors::PhinsBin::setupConnections();

  // we re-use the instrument connection.  But it's bi-directional now.
  instrument = connections().at("instrument");

  auto use_repeater = ros::param::param<bool>("~use_repeater", false);
  if (use_repeater)
  {
    ROS_INFO_STREAM("Connecting to PHINS repeater port...");
    repeater = addConnection("repeater", boost::bind(&PhinsIns::parseReceivedRepeater, this, _1));
  }
}

std::string PhinsIns::appendChecksum(const std::string& base) {
  std::stringstream ret;
  unsigned int chksum = ds_nmea_msgs::calculate_checksum(base);
  ret <<base <<std::hex <<std::setfill('0') <<std::setw(2) <<chksum <<"\r\n";

  //ROS_INFO_STREAM("Preparing to send query string: " <<ret.str());
  return ret.str();
}

void PhinsIns::setupTimers() {
  ds_sensors::PhinsBin::setupTimers();

  // add some queries
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,ZUP___,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,START_,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,ALTMDE,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,GPSKFM,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,GP2KFM,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,LOGKFM,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,LOGWTM,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,LG2KFM,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,LG2WTM,,*"));
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,DEPKFM,,*"));
  // not sure what to do about USBL.. let's do one for now
  queries_.push_back(appendChecksum("$PIXSE,CONFIG,USBKFM,,0*"));

  auto nh = nodeHandle();
  query_timer_ = nh.createTimer(ros::Duration(0.2), &PhinsIns::sendNextCommand, this);
}

void PhinsIns::onGps1(const sensor_msgs::NavSatFix& gps) {
  last_gps1_ = gps;
  sendExtSensorMessage(gpsToExtbin(gps, gps1_params_));
}

void PhinsIns::onGps2(const sensor_msgs::NavSatFix& gps) {
  last_gps2_ = gps;
  sendExtSensorMessage(gpsToExtbin(gps, gps2_params_));
}

void PhinsIns::onDvl1(const ds_sensor_msgs::Dvl& dvl) {
  sendExtSensorMessage(dvlToExtbin(dvl, dvl1_params_));
}

void PhinsIns::onDvl2(const ds_sensor_msgs::Dvl& dvl) {
  sendExtSensorMessage(dvlToExtbin(dvl, dvl2_params_));
}

void PhinsIns::onDvlWt1(const ds_sensor_msgs::Dvl& dvl) {
  sendExtSensorMessage(dvlWtToExtbin(dvl, dvlWt1_params_));
}

void PhinsIns::onDvlWt2(const ds_sensor_msgs::Dvl& dvl) {
  sendExtSensorMessage(dvlWtToExtbin(dvl, dvlWt2_params_));
}

void PhinsIns::onDepth(const ds_sensor_msgs::DepthPressure& dep) {
  sendExtSensorMessage(depthToExtbin(dep, depth_params_));
}

void PhinsIns::onUsbl(const sensor_msgs::NavSatFix& usbl) {
  last_usbl_ = usbl;
  sendExtSensorMessage(usblToExtbin(usbl, usbl_params_));
}

void PhinsIns::onSoundspeed(const ds_sensor_msgs::SoundSpeed &sspeed) {
  sendExtSensorMessage(soundspeedToExtbin(sspeed, sv_min, sv_max));
}

void PhinsIns::onExtGps(const ds_nmea_msgs::Gga& ext_gga) {
	int lat_dir;
	int lon_dir;
	//Read Lat Direction
	if (ext_gga.latitude_dir == int('S')) {
		lat_dir = -1;
	}
	else {
		lat_dir = 1;
	}
	
	declat = lat_dir * ext_gga.latitude;
	
	// Read Lon Direction
	if (ext_gga.longitude_dir == int('W')) {
		lon_dir = -1;
	}
        else {
		lon_dir = 1;
	}
	declon = lon_dir * ext_gga.longitude;
	antenna_alt = ext_gga.antenna_alt;
}

bool PhinsIns::rebootCallback(ds_core_msgs::VoidCmd::Request &req, ds_core_msgs::VoidCmd::Response &resp) {

  ROS_INFO_STREAM("Restarting PHINS via service request");

  // save parameters to PROM:
  repeater->send(appendChecksum("$PIXSE,CONFIG,SAVE__*"));

  // initiate a reboot after seeing the save acknowledged
  restart_in_progress_ = true;
  resp.msg = "Reboot initiated";
  resp.success = true;

  return true;
}

bool PhinsIns::reinitCallback(ds_core_msgs::StringCmd::Request &req, ds_core_msgs::StringCmd::Response &resp) { 
  ROS_ERROR_STREAM("Re-initializing PHINS to: "<<req.cmd<<" via service request");
  //handle wait for pos request
  if (std::stoi(req.cmd) == 0) {
	ROS_WARN_STREAM("Setting PHINS Startup Mode: WAIT FOR POSITION");
	repeater->send(appendChecksum("$PIXSE,CONFIG,START_,1*")); //change startup to wait for position
	repeater->send(appendChecksum("$PIXSE,CONFIG,SAVE__*")); //save new config
	restart_in_progress_ = true; //initiate a restart
	resp.msg = "Reinit to Wait for Position Mode";
        resp.success = true;
	return true;
  }
  //handle restore last pos request
  else if (std::stoi(req.cmd) == 1) {
	ROS_WARN_STREAM("Setting PHINS Startup Mode: RESTORE LAST POSITION");
	repeater->send(appendChecksum("$PIXSE,CONFIG,START_,0*")); //change startup to immediate run (cant go wait for pos straight to restore last pos)
	repeater->send(appendChecksum("$PIXSE,CONFIG,START_,2*")); //now change from immediate run to restore last pos
	repeater->send(appendChecksum("$PIXSE,CONFIG,SAVE__*")); //save new config
	restart_in_progress_ = true; //initiate a restart
	resp.msg = "Reinit to Restore Last Position Mode";
	resp.success = true;
	return true;
  }
  //handle immediate run request
  else if (std::stoi(req.cmd) == 2) {
	ROS_WARN_STREAM("Setting PHINS Startup Mode: IMMEDIATE RUN");
	repeater->send(appendChecksum("$PIXSE,CONFIG,START_,0*")); //change startup to immediate run (cant go wait for pos straight to restore last pos)
	repeater->send(appendChecksum("$PIXSE,CONFIG,SAVE__*")); //save new config
	restart_in_progress_ = true; //initiate a restart
	resp.msg = "Reinit to Restore Last Position Mode";
	resp.success = true;
	return true;  
  }
  else {
  	ROS_ERROR_STREAM("Unknown PHINS startup mode requested!");
	resp.msg = "Reinit FAILED";
	resp.success = false;
	return false;
	}
}

bool PhinsIns::manposCallback(ds_core_msgs::VoidCmd::Request &req, ds_core_msgs::VoidCmd::Response &resp) {
	ROS_WARN_STREAM("Setting PHINS Manual Position");

	// Get a GPS fix
	std::ostringstream manpos_os;
	manpos_os<<"$PIXSE,CONFIG,MANPOS,"<<declat<<","<<declon<<","<<antenna_alt;
        std::string manposstr(manpos_os.str()); 	
	// send manual position to repeater
	ROS_ERROR_STREAM("SENDING PHINS MANPOS: "<<manposstr);
	repeater->send(appendChecksum(manposstr));
	repeater->send(appendChecksum("$PIXSE,CONFIG,SAVE__*")); //Save new config
	restart_in_progress_ = true; //initiate a restart
	resp.msg = "PHINS Manual Position set: rebooting";
	resp.success = true;
	return true;
 }

bool PhinsIns::usblModeCallback(ds_core_msgs::StringCmd::Request &req, ds_core_msgs::StringCmd::Response &resp) {
  if (std::stoi(req.cmd) <= 5) {
    usbl_params_.covariance_scale_factor = std::stoi(req.cmd);
    resp.success = true;
    ROS_WARN_STREAM("Set PHINS USBL Covariance Scalar to: "<<std::stoi(req.cmd));
    return true;
  }
  else {
    usbl_params_.covariance_scale_factor = 1;
    resp.msg = "PHINS USBL mode: UKNOWN";
    resp.success = false;
    return false;
  }
}
void PhinsIns::sendExtSensorMessage(const std::string& msg) {
  if (msg.empty()) {
    return;
  }

  if (!instrument) {
    ROS_ERROR_STREAM("No connection to PHINS available, NOT SENDING external sensor data...");
    return;
  }
  instrument->send(msg);
}

std::string PhinsIns::gpsToExtbin(const sensor_msgs::NavSatFix& fix, const GpsParams& params) {

  // sanity check first
  if (!( std::isfinite(fix.longitude) && std::isfinite(fix.latitude) && std::isfinite(fix.altitude))) {
    ROS_INFO_STREAM("Dropping GPS fix with non-finite field!");
    return "";
  }

  uint8_t buf[sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalGps) + 4];
  memset(buf, 0, sizeof(buf));
  _phinsstdbin3::InputHeader* hdr = reinterpret_cast<_phinsstdbin3::InputHeader*>(buf);
  _phinsstdbin3::ExternalGps* ext_gps = reinterpret_cast<_phinsstdbin3::ExternalGps*>(
      buf + sizeof(_phinsstdbin3::InputHeader));
  size_t pktsize = sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalGps);
  uint32_t* chksum = reinterpret_cast<uint32_t*>(buf + pktsize);
  hdr->header1 = _phinsstdbin3::HEADER1;
  hdr->header2 = _phinsstdbin3::HEADER2;
  hdr->version = _phinsstdbin3::VERSION3;
  hdr->nav_data_mask = 0x0;
  hdr->nav_extended_data_mask = 0x0;
  switch (params.gps_id) {
    case _phinsstdbin3::gps_id::gps1:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_GPS1);
      break;
    case _phinsstdbin3::gps_id::gps2:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_GPS2);
      break;
    case _phinsstdbin3::gps_id::gps_man:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_GPS_MANUAL);
      break;
  }
  hdr->total_size = htobe16(pktsize + sizeof(uint32_t));
  hdr->time_ref = _phinsstdbin3::time_ref::UTC;

  uint8_t quality = _phinsstdbin3::gps_quality::invalid;

  switch (fix.status.status) {
    case sensor_msgs::NavSatStatus::STATUS_NO_FIX:
      quality = _phinsstdbin3::gps_quality::invalid;
      break;
    case sensor_msgs::NavSatStatus::STATUS_FIX:
      quality = _phinsstdbin3::gps_quality::natural;
      break;
    case sensor_msgs::NavSatStatus::STATUS_SBAS_FIX:
      quality = _phinsstdbin3::gps_quality::differential;
      break;
    case sensor_msgs::NavSatStatus::STATUS_GBAS_FIX:
      quality = _phinsstdbin3::gps_quality::float_rtk;
      break;
  }

  ext_gps->validity_time = htobe32(rostime2phinstime(fix.header.stamp));
  ext_gps->gps_id = params.gps_id;
  ext_gps->gps_quality = quality;
  ext_gps->latitude = hdouble_to_be64(fix.latitude);
  ext_gps->longitude = hdouble_to_be64(fix.longitude);
  ext_gps->altitude = hfloat_to_be32(fix.altitude);

  if (std::isfinite(params.lat_stddev) && std::isfinite(params.lon_stddev)) {
    ROS_INFO_STREAM_ONCE("Using ROSPARAM lat/lon uncertainties: lon: "
                             <<params.lon_stddev <<" / lat: " <<params.lat_stddev);
    ext_gps->latitude_stddev = hfloat_to_be32(params.lat_stddev);
    ext_gps->longitude_stddev = hfloat_to_be32(params.lon_stddev);
    ext_gps->lat_lon_covar = 0;
  } else {
    ROS_INFO_STREAM_ONCE("Using GPS message horizontal accuracy for PHINS");
    ext_gps->latitude_stddev = hfloat_to_be32(sqrt(fix.position_covariance[0]));
    ext_gps->longitude_stddev = hfloat_to_be32(sqrt(fix.position_covariance[4]));
    ext_gps->lat_lon_covar = 0;
  }
  if (std::isfinite(params.alt_stddev)) {
    ROS_INFO_STREAM_ONCE("Using ROSPARAM altitude uncertainty: " <<params.alt_stddev);
    ext_gps->altitude_stddev = hfloat_to_be32(params.alt_stddev);
  } else {
    ROS_INFO_STREAM_ONCE("Using GPS message vertical accuracy for PHINS");
    ext_gps->altitude_stddev = hfloat_to_be32(sqrt(fix.position_covariance[8]));
  }

  ext_gps->geoid_separation = hfloat_to_be32(0); // no support for this in the NavSatFix message.  Grrrr.

  // fill in a checksum, or the Phins won't accept the datagram
  *chksum = htobe32(calc_checksum(buf, pktsize));
  return std::string(reinterpret_cast<const char*>(buf), pktsize + sizeof(uint32_t));
}

std::string PhinsIns::usblToExtbin(const sensor_msgs::NavSatFix& fix, const UsblParams& params) {
  // sanity check first
  if (!( std::isfinite(fix.longitude) && std::isfinite(fix.latitude) && std::isfinite(fix.altitude))) {
    return "";
  }

  uint8_t buf[sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalUsbl) + 4];
  memset(buf, 0, sizeof(buf));
  _phinsstdbin3::InputHeader* hdr = reinterpret_cast<_phinsstdbin3::InputHeader*>(buf);
  _phinsstdbin3::ExternalUsbl* ext_usbl = reinterpret_cast<_phinsstdbin3::ExternalUsbl*>(
      buf + sizeof(_phinsstdbin3::InputHeader));
  size_t pktsize = sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalUsbl);
  uint32_t* chksum = reinterpret_cast<uint32_t*>(buf + pktsize);
  hdr->header1 = _phinsstdbin3::HEADER1;
  hdr->header2 = _phinsstdbin3::HEADER2;
  hdr->version = _phinsstdbin3::VERSION3;
  hdr->nav_data_mask = 0x0;
  hdr->nav_extended_data_mask = 0x0;
  switch (params.usbl_id) {
    case _phinsstdbin3::usbl_id::usbl1:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_USBL1);
      break;
    case _phinsstdbin3::usbl_id::usbl2:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_USBL2);
      break;
    case _phinsstdbin3::usbl_id::usbl3:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_USBL3);
      break;
  }
  hdr->total_size = htobe16(pktsize + sizeof(uint32_t));
  hdr->time_ref = _phinsstdbin3::time_ref::UTC;

  uint8_t quality = _phinsstdbin3::gps_quality::invalid;

  switch (fix.status.status) {
    case sensor_msgs::NavSatStatus::STATUS_NO_FIX:
      quality = _phinsstdbin3::gps_quality::invalid;
      break;
    case sensor_msgs::NavSatStatus::STATUS_FIX:
      quality = _phinsstdbin3::gps_quality::natural;
      break;
    case sensor_msgs::NavSatStatus::STATUS_SBAS_FIX:
      quality = _phinsstdbin3::gps_quality::differential;
      break;
    case sensor_msgs::NavSatStatus::STATUS_GBAS_FIX:
      quality = _phinsstdbin3::gps_quality::float_rtk;
      break;
  }

  ext_usbl->validity_time = htobe32(rostime2phinstime(fix.header.stamp));
  ext_usbl->usbl_id = params.usbl_id;
  //ext_usbl->beacon_id = params.
  ext_usbl->latitude = hdouble_to_be64(fix.latitude);
  ext_usbl->longitude = hdouble_to_be64(fix.longitude);
  ext_usbl->altitude = hfloat_to_be32(fix.altitude);

  if (std::isfinite(params.lat_stddev) && std::isfinite(params.lon_stddev)) {
    ROS_INFO_STREAM_ONCE("Using ROSPARAM lat/lon uncertainties: lon: "
                             <<params.lon_stddev <<" / lat: " <<params.lat_stddev);
    ext_usbl->north_stddev = hfloat_to_be32(params.lat_stddev);
    ext_usbl->east_stddev = hfloat_to_be32(params.lon_stddev);
    ext_usbl->latlon_covar = 0;
  } else {
    ROS_INFO_STREAM_ONCE("Using USBL message horizontal accuracy for PHINS");
    ROS_INFO_STREAM_ONCE("USBL COVARIANCE SCALE FACTOR: "<<params.covariance_scale_factor);
    ext_usbl->north_stddev = hfloat_to_be32(sqrt(fix.position_covariance[0]) * params.covariance_scale_factor);
    ext_usbl->east_stddev = hfloat_to_be32(sqrt(fix.position_covariance[4]) * params.covariance_scale_factor);
    ext_usbl->latlon_covar = 0;
  }
  if (std::isfinite(params.alt_stddev)) {
    ROS_INFO_STREAM_ONCE("Using ROSPARAM altitude uncertainty: " <<params.alt_stddev);
    ext_usbl->altitude_stddev = hfloat_to_be32(params.alt_stddev);
  } else {
    ROS_INFO_STREAM_ONCE("Using GPS message vertical accuracy for PHINS");
    ext_usbl->altitude_stddev = hfloat_to_be32(sqrt(fix.position_covariance[8]));
  }

  // fill in a checksum, or the Phins won't accept the datagram
  *chksum = htobe32(calc_checksum(buf, pktsize));
  return std::string(reinterpret_cast<const char*>(buf), pktsize + sizeof(uint32_t));
}

std::string PhinsIns::dvlToExtbin(const ds_sensor_msgs::Dvl& dvl, const DvlParams& params) {
  // sanity check first
  if (!( dvl.num_good_beams >= params.min_beams && std::isfinite(dvl.velocity.x)
      && std::isfinite(dvl.velocity.y) && std::isfinite(dvl.velocity.z))) {
    return "";
  }


  uint8_t buf[sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalGroundspeed) + 4];
  memset(buf, 0, sizeof(buf));
  _phinsstdbin3::InputHeader* hdr = reinterpret_cast<_phinsstdbin3::InputHeader*>(buf);
  _phinsstdbin3::ExternalGroundspeed* ext_dvl = reinterpret_cast<_phinsstdbin3::ExternalGroundspeed*>(
      buf + sizeof(_phinsstdbin3::InputHeader));
  size_t pktsize = sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalGroundspeed);
  uint32_t* chksum = reinterpret_cast<uint32_t*>(buf + pktsize);
  hdr->header1 = _phinsstdbin3::HEADER1;
  hdr->header2 = _phinsstdbin3::HEADER2;
  hdr->version = _phinsstdbin3::VERSION3;
  hdr->nav_data_mask = 0x0;
  hdr->nav_extended_data_mask = 0x0;
  hdr->total_size = htobe16(pktsize + sizeof(uint32_t));
  hdr->time_ref = _phinsstdbin3::time_ref::UTC;

  switch (params.dvl_id) {
    case _phinsstdbin3::dvl_id::dvl1:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_GROUND_DVL1);
      break;
    case _phinsstdbin3::dvl_id::dvl2:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_GROUND_DVL2);
      break;
  }
  ext_dvl->dvl_id = params.dvl_id;
  ext_dvl->validity_time = htobe32(rostime2phinstime(dvl.header.stamp));

  if (params.swap_xy)
    {
      ext_dvl->speed_xv1 = hfloat_to_be32(params.xs1_scale_factor * dvl.velocity.y);
      ext_dvl->speed_xv2 = hfloat_to_be32(params.xs2_scale_factor * dvl.velocity.x);
      ext_dvl->speed_xv3 = hfloat_to_be32(params.xs3_scale_factor * dvl.velocity.z);
    }
  else
    {
      ext_dvl->speed_xv1 = hfloat_to_be32(params.xs1_scale_factor * dvl.velocity.x);
      ext_dvl->speed_xv2 = hfloat_to_be32(params.xs2_scale_factor * dvl.velocity.y);
      ext_dvl->speed_xv3 = hfloat_to_be32(params.xs3_scale_factor * dvl.velocity.z);
    }
  
  ext_dvl->speed_sound = hfloat_to_be32(dvl.speed_sound);
  ext_dvl->altitude = hfloat_to_be32(dvl.altitude);

  if (std::isfinite(params.xs1_stddev)
      && std::isfinite(params.xs2_stddev)
      && std::isfinite(params.xs3_stddev)) {
    ROS_INFO_STREAM_ONCE("Using ROSPARAM dvl xs1 stddev=" <<params.xs1_stddev);
    ROS_INFO_STREAM_ONCE("Using ROSPARAM dvl xs2 stddev=" <<params.xs2_stddev);
    ROS_INFO_STREAM_ONCE("Using ROSPARAM dvl xs3 stddev=" <<params.xs3_stddev);
    ext_dvl->speed_xv1_stddev = hfloat_to_be32(params.xs1_stddev);
    ext_dvl->speed_xv2_stddev = hfloat_to_be32(params.xs2_stddev);
    ext_dvl->speed_xv3_stddev = hfloat_to_be32(params.xs3_stddev);
  } else {
    ROS_FATAL_STREAM("No DVL uncertainty specified!");
    ROS_BREAK();
  }

  // fill in a checksum, or the Phins won't accept the datagram
  *chksum = htobe32(calc_checksum(buf, pktsize));
  return std::string(reinterpret_cast<const char*>(buf), pktsize+sizeof(uint32_t));
}

std::string PhinsIns::dvlWtToExtbin(const ds_sensor_msgs::Dvl& dvl, const DvlParams& params) {
  // sanity check first
  if (!( dvl.num_good_beams >= params.min_beams && std::isfinite(dvl.velocity.x)
      && std::isfinite(dvl.velocity.y) && std::isfinite(dvl.velocity.z))) {
    return "";
  }
  uint8_t buf[sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalWaterspeed) + 4];
  memset(buf, 0, sizeof(buf));
  _phinsstdbin3::InputHeader* hdr = reinterpret_cast<_phinsstdbin3::InputHeader*>(buf);
  _phinsstdbin3::ExternalWaterspeed* ext_dvl = reinterpret_cast<_phinsstdbin3::ExternalWaterspeed*>(
      buf + sizeof(_phinsstdbin3::InputHeader));
  size_t pktsize = sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalWaterspeed);
  uint32_t* chksum = reinterpret_cast<uint32_t*>(buf + pktsize);
  hdr->header1 = _phinsstdbin3::HEADER1;
  hdr->header2 = _phinsstdbin3::HEADER2;
  hdr->version = _phinsstdbin3::VERSION3;
  hdr->nav_data_mask = 0x0;
  hdr->nav_extended_data_mask = 0x0;
  hdr->total_size = htobe16(pktsize + sizeof(uint32_t));
  hdr->time_ref = _phinsstdbin3::time_ref::UTC;

  switch (params.dvl_id) {
    case _phinsstdbin3::dvl_id::dvl1:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_WATER_DVL1);
      break;
    case _phinsstdbin3::dvl_id::dvl2:
      hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_WATER_DVL2);
      break;
  }
  ext_dvl->dvl_id = params.dvl_id;
  ext_dvl->validity_time = htobe32(rostime2phinstime(dvl.header.stamp));

  if (params.swap_xy)                 
    {
      ext_dvl->speed_xv1 = hfloat_to_be32(params.xs1_scale_factor * dvl.velocity.y);
      ext_dvl->speed_xv2 = hfloat_to_be32(params.xs2_scale_factor * dvl.velocity.x);
      ext_dvl->speed_xv3 = hfloat_to_be32(params.xs3_scale_factor * dvl.velocity.z);
    }
  else
    {
      ext_dvl->speed_xv1 = hfloat_to_be32(params.xs1_scale_factor * dvl.velocity.x);
      ext_dvl->speed_xv2 = hfloat_to_be32(params.xs2_scale_factor * dvl.velocity.y);
      ext_dvl->speed_xv3 = hfloat_to_be32(params.xs3_scale_factor * dvl.velocity.z);
    }

  ext_dvl->speed_sound = hfloat_to_be32(dvl.speed_sound);

  if (std::isfinite(params.xs1_stddev)
      && std::isfinite(params.xs2_stddev)
      && std::isfinite(params.xs3_stddev)) {
    ROS_INFO_STREAM_ONCE("Using ROSPARAM dvl xs1 stddev=" <<params.xs1_stddev);
    ROS_INFO_STREAM_ONCE("Using ROSPARAM dvl xs2 stddev=" <<params.xs2_stddev);
    ROS_INFO_STREAM_ONCE("Using ROSPARAM dvl xs3 stddev=" <<params.xs3_stddev);
    ext_dvl->speed_xv1_stddev = hfloat_to_be32(params.xs1_stddev);
    ext_dvl->speed_xv2_stddev = hfloat_to_be32(params.xs2_stddev);
    ext_dvl->speed_xv3_stddev = hfloat_to_be32(params.xs3_stddev);
  } else {
    ROS_FATAL_STREAM("No DVL uncertainty specified!");
    ROS_BREAK();
  }

  // fill in a checksum, or the Phins won't accept the datagram
  *chksum = htobe32(calc_checksum(buf, pktsize));
  return std::string(reinterpret_cast<const char*>(buf), pktsize+sizeof(uint32_t));
}

std::string PhinsIns::depthToExtbin(const ds_sensor_msgs::DepthPressure& depth, const DepthParams& params) {
  // sanity check first
  if (!( std::isfinite(depth.depth))) {
    return "";
  }

  uint8_t buf[sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalDepth) + 4];
  memset(buf, 0, sizeof(buf));
  _phinsstdbin3::InputHeader* hdr = reinterpret_cast<_phinsstdbin3::InputHeader*>(buf);
  _phinsstdbin3::ExternalDepth* ext_dep = reinterpret_cast<_phinsstdbin3::ExternalDepth*>(
      buf + sizeof(_phinsstdbin3::InputHeader));
  size_t pktsize = sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalDepth);
  uint32_t* chksum = reinterpret_cast<uint32_t*>(buf + pktsize);
  hdr->header1 = _phinsstdbin3::HEADER1;
  hdr->header2 = _phinsstdbin3::HEADER2;
  hdr->version = _phinsstdbin3::VERSION3;
  hdr->nav_data_mask = 0x0;
  hdr->nav_extended_data_mask = 0x0;
  hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_DEPTH);
  hdr->total_size = htobe16(pktsize + sizeof(uint32_t));
  hdr->time_ref = _phinsstdbin3::time_ref::UTC;

  ext_dep->validity_time = htobe32(rostime2phinstime(depth.header.stamp));
  ext_dep->depth = hfloat_to_be32(depth.depth);

  if (std::isfinite(params.depth_stddev)) {
    ROS_INFO_STREAM_ONCE("Using ROSPARAM depth stddev=" <<params.depth_stddev);
    ext_dep->depth_stddev = hfloat_to_be32(params.depth_stddev);
  } else {
    if (depth.pressure_covar <= 0) {
      ROS_WARN_STREAM_ONCE("No ROSPARAM depth covar available, message value is invalid.  Marking message as invalid to phins");
      ext_dep->depth_stddev = hfloat_to_be32(sqrt(depth.pressure_covar));
    }
  }

  // fill in a checksum, or the Phins won't accept the datagram
  *chksum = htobe32(calc_checksum(buf, pktsize));
  return std::string(reinterpret_cast<const char*>(buf), pktsize+sizeof(uint32_t));
}

std::string PhinsIns::soundspeedToExtbin(const ds_sensor_msgs::SoundSpeed &sspeed, double sv_min, double sv_max) {

  if (sspeed.speed < sv_min || sspeed.speed > sv_max || !std::isfinite(sspeed.speed)) {
    return "";
  }

  uint8_t buf[sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalSoundspeed) + 4];
  memset(buf, 0, sizeof(buf));
  _phinsstdbin3::InputHeader* hdr = reinterpret_cast<_phinsstdbin3::InputHeader*>(buf);
  _phinsstdbin3::ExternalSoundspeed* ext_sspeed = reinterpret_cast<_phinsstdbin3::ExternalSoundspeed*>(
      buf + sizeof(_phinsstdbin3::InputHeader));
  size_t pktsize = sizeof(_phinsstdbin3::InputHeader) + sizeof(_phinsstdbin3::ExternalSoundspeed);
  uint32_t* chksum = reinterpret_cast<uint32_t*>(buf + pktsize);
  hdr->header1 = _phinsstdbin3::HEADER1;
  hdr->header2 = _phinsstdbin3::HEADER2;
  hdr->version = _phinsstdbin3::VERSION3;
  hdr->nav_data_mask = 0x0;
  hdr->nav_extended_data_mask = 0x0;
  hdr->external_data_mask = htobe32(_phinsstdbin3::EXTERNAL_SOUNDSPEED);
  hdr->total_size = htobe16(pktsize + sizeof(uint32_t));
  hdr->time_ref = _phinsstdbin3::time_ref::UTC;

  ext_sspeed->validity_time = htobe32(rostime2phinstime(sspeed.header.stamp));
  ext_sspeed->sound_speed = hfloat_to_be32(sspeed.speed);

  // fill in a checksum, or the Phins won't accept the datagram
  *chksum = htobe32(calc_checksum(buf, pktsize));
  return std::string(reinterpret_cast<const char*>(buf), pktsize+sizeof(uint32_t));
}

void PhinsIns::sendNextCommand(const ros::TimerEvent& evt) {
  repeater->send(queries_[query_idx_]);
  query_idx_++;
  if (query_idx_ >= queries_.size()) {
    query_idx_ = 0;
    send_on_next_config_ = true;
    dr_srv_->updateConfig(dynamic_config_);
  }
}

void PhinsIns::parseReceivedRepeater(const ds_core_msgs::RawData& bytes)
{
  std::string nmea_msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size());

  size_t dollar = nmea_msg.find('$');
  if (dollar == std::string::npos) {
    return;
  }
  size_t first_comma = nmea_msg.find(',', dollar+1);
  if (first_comma == std::string::npos) {
    return;
  }
  size_t second_comma = nmea_msg.find(',', first_comma+1);
  if (second_comma == std::string::npos) {
    return;
  }
  std::string talker = nmea_msg.substr(dollar+1, first_comma - dollar - 1);
  std::string msgtype = nmea_msg.substr(first_comma+1, second_comma - first_comma - 1);

  if (talker != "PIXSE") {
    // skip stuff that isn't ixblue-proprietary
    return;
  }
  if (msgtype == "ACCEST")
  {
    // Accelerometers bias
    // $PIXSE,ACCEST,-754.7915,31.4370,149.9561*5E
    //ROS_ERROR_STREAM("Found accest: " << nmea_msg);
    auto msg = ds_nmea_msgs::PixseAccest{};
    if (!ds_nmea_msgs::from_string(msg, nmea_msg))
    {
      ROS_WARN_STREAM("Unable to parse $PIXSE,ACCEST from string: " << nmea_msg);
      return;
    }
    geometry_msgs::Vector3Stamped accest;
    accest.header.stamp = ros::Time::now();
    accest.vector.x = msg.x;
    accest.vector.y = msg.y;
    accest.vector.z = msg.z;
    accest_pub_.publish(accest);
  }
  else if (msgtype == "FOGEST") {
    //ROS_ERROR_STREAM("Found fogest: " << nmea_msg);
    auto msg = ds_nmea_msgs::PixseFogest{};
    if (!ds_nmea_msgs::from_string(msg, nmea_msg)) {
      ROS_WARN_STREAM("Unable to parse $PIXSE,FOGEST from string: " << nmea_msg);
      return;
    }
    geometry_msgs::Vector3Stamped fogest;
    fogest.header.stamp = ros::Time::now();
    fogest.vector.x = msg.x;
    fogest.vector.y = msg.y;
    fogest.vector.z = msg.z;
    fogest_pub_.publish(fogest);
  } else if (msgtype == "CONFIG") {
    parseConfig(nmea_msg, second_comma);
    if (send_on_next_config_) {
      send_on_next_config_ = false;
      dr_srv_->updateConfig(dynamic_config_);
    }

  } else if (msgtype == "ATITUD" || msgtype == "POSITI" || msgtype == "SPEED_" || msgtype == "UTMWGS"
                                 || msgtype == "HEAVE_" || msgtype == "TIME__" || msgtype == "LOGEST"
                                 || msgtype == "ALGSTS" || msgtype == "STATUS" || msgtype == "HT_STS"
                                 || msgtype == "SORSTS" || msgtype == "STDPOS" || msgtype == "STDHRP"
                                 || msgtype == "STDSPD" || msgtype == "GPSIN_") {
    // skip these
  } else {
    ROS_INFO_STREAM("REPEATER: \"" << nmea_msg.substr(0, nmea_msg.length() - 2) << "\"");
  }

}

void PhinsIns::parseConfig(const std::string& message, size_t comma_idx) {
  // we need the config value:
  size_t second_comma = message.find(',', comma_idx+1);
  if (second_comma == std::string::npos) {
    return;
  }

  std::string param_name = message.substr(comma_idx+1, second_comma - comma_idx - 1);
  if (param_name == "ZUP___") {
    updateInt(dynamic_config_.zuptMode, message, second_comma);

  } else if (param_name == "ALTMDE") {
    updateInt(dynamic_config_.altMode, message, second_comma);

  } else if (param_name == "GPSKFM") {
    updateInt(dynamic_config_.gps1Rejection, message, second_comma);

  } else if (param_name == "GP2KFM") {
    updateInt(dynamic_config_.gps2Rejection, message, second_comma);

  } else if (param_name == "LOGKFM") {
    updateInt(dynamic_config_.dvl1Rejection, message, second_comma);

  } else if (param_name == "LOGWTM") {
    updateInt(dynamic_config_.dvlWt1Rejection, message, second_comma);

  } else if (param_name == "LG2KFM") {
    updateInt(dynamic_config_.dvl2Rejection, message, second_comma);

  } else if (param_name == "LG2WTM") {
    updateInt(dynamic_config_.dvlWt2Rejection, message, second_comma);

  } else if (param_name == "DEPKFM") {
    updateInt(dynamic_config_.depthRejection, message, second_comma);

  } else if (param_name == "USBKFM") {
    updateInt(dynamic_config_.usblRejection, message, second_comma);

  }else if (param_name == "SAVE__") {
    ROS_INFO_STREAM(ros::this_node::getName() <<" Config saved.");
    if (restart_in_progress_) {
      ROS_INFO_STREAM(ros::this_node::getName() <<" Restarting PHINS!");
      repeater->send(appendChecksum("$PIXSE,CONFIG,RESET_*"));
      restart_in_progress_ = false;
    }
  }else if (param_name == "START_") {
	  ROS_WARN_STREAM_ONCE("PHINS Start Mode Config: "<<message);
  } else {
    ROS_WARN_STREAM(ros::this_node::getName() + ": Unknown CONFIG key=\"" <<param_name <<"\", skipping");
  }
}

void PhinsIns::updateInt(int& dest, const std::string& message, size_t comma_idx) {
  size_t delim = message.find(',', comma_idx+1);
  if (delim == std::string::npos) {
    delim = message.find('*', comma_idx+1);
  }
  if (delim == std::string::npos) {
    // if we STILL can't figure it out, just abort
    return;
  }
  std::string field = message.substr(comma_idx+1, delim - comma_idx - 1);
  try {
    dest = std::stol(field);
  } catch( const std::invalid_argument& err) {
    ROS_INFO_STREAM("Could not convert INT field: \"" <<message <<"\" idx=" <<comma_idx);
  }
}


void PhinsIns::dynamicParamCallback(ds_sensor_msgs::PhinsConfig &config, uint32_t level) {
  if (dynamic_config_.zuptMode != config.zuptMode) {
    sendCommandInt("ZUP___", config.zuptMode);
  }

  if (dynamic_config_.altMode != config.altMode) {
     sendCommandInt("ALTMDE", config.altMode);
  };

  if (dynamic_config_.gps1Rejection != config.gps1Rejection) {
    sendCommandInt("GPSKFM", config.gps1Rejection);
  }

  if (dynamic_config_.gps2Rejection != config.gps2Rejection) {
    sendCommandInt("GP2KFM", config.gps2Rejection);
  }

  if (dynamic_config_.dvl1Rejection != config.dvl1Rejection) {
    sendCommandInt("LOGKFM", config.dvl1Rejection);
  }

  if (dynamic_config_.dvlWt1Rejection != config.dvlWt1Rejection) {
    sendCommandInt("LOGWTM", config.dvlWt1Rejection);
  }

  if (dynamic_config_.dvl2Rejection != config.dvl2Rejection) {
    sendCommandInt("LG2KFM", config.dvl2Rejection);
  }

  if (dynamic_config_.dvlWt2Rejection != config.dvlWt2Rejection) {
    sendCommandInt("LG2WTM", config.dvlWt2Rejection);
  }

  if (dynamic_config_.depthRejection != config.depthRejection) {
    sendCommandInt("DEPKFM", config.depthRejection);
  }

  if (dynamic_config_.usblRejection != config.usblRejection) {
    sendCommandTwoInts("USBKFM", config.usblRejection, 0);
  }

  // Default diveMode config is None
  dynamic_config_.diveMode = 2;

  dynamic_config_ = config;
}

void PhinsIns::sendCommandInt(const std::string& key, int value) {
  std::stringstream builder;

  builder <<"$PIXSE,CONFIG," <<key <<"," <<value <<"*";

  std::string to_send = appendChecksum(builder.str());
  ROS_INFO_STREAM("Sending command to PHINS on repeater port: \"" <<to_send <<"\"");
  repeater->send(to_send);

  // hopefully the next message will be a reply
  send_on_next_config_ = true;
}

void PhinsIns::sendCommandTwoInts(const std::string& key, int v1, int v2) {
  std::stringstream builder;

  builder <<"$PIXSE,CONFIG," <<key <<"," <<v1 <<"," <<v2 <<"*";

  std::string to_send = appendChecksum(builder.str());
  ROS_INFO_STREAM("Sending command to PHINS on repeater port: \"" <<to_send <<"\"");
  repeater->send(to_send);

  // hopefully the next message will be a reply
  send_on_next_config_ = true;
}

} // namespace ds_sensors
