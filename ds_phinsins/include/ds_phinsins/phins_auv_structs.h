//
// Created by ivaughn on 1/3/20.
//

#ifndef PHINSINS_PHINS_AUV_STRUCTS_H_
#define PHINSINS_PHINS_AUV_STRUCTS_H_

#include <stdint.h>
#include <limits>

namespace phinsins {

// The IXSEA AUV format is noted as being used by GAPS.  As this is also an
// IxBlue product, it seems unusually likely to be tested

namespace ixsea_auv {

// ////////////////////////////////////////////////////////////////////////// //
// GPS
struct gps {
  char byte;
  uint8_t size;
  uint8_t id;
  uint8_t version;
  uint32_t time_tag; // 100us
  uint8_t quality;
  int32_t latitude; // +/- 2^31 = +/- pi radians
  int32_t longitude; // +/- 2^31 = +/- pi radians
  float altitude; // meters, referenced to mean sea level
  float latitude_stddev; // meters
  float longitude_stddev; // meters
  float altitude_stddev; // meters
  uint8_t validity; // 1 = valid, 0 = invalid
  float lat_lon_covar; // meters
  float latency; // seconds
  float geoid_separation; //meters, geoid - ellipsoid
  uint16_t checksum; // unsigned sum
} __attribute__((packed));

// NOTE: As per page 120 of the INS InterfaceLibrary, when the INS is managing the
// altitude, GPS position should be referenced to mean sea level and geoid separation should be
// the "difference between geoid and ellipsoid at current position."  If the INS is NOT managing
// altitude, the altitude field is referenced from the ellipsoid and geoidal separation field
// contains NaN value 0x7FC00000

// ////////////////////////////////////////////////////////////////////////// //
// DVL
struct ground_speed {
  char byte;
  uint8_t size;
  uint8_t id;
  uint8_t version;
  uint32_t time_tag; // 100us
  float xs1_speed; // m/s
  float xs2_speed; // m/s
  float xs3_speed; // m/s
  float dvl_soundspeed; // m/s, or NaN
  float external_soundspeed; // m/s, or NaN
  float dvl_altitude; // meters
  float xs1_speed_stddev; // m/s
  float xs2_speed_stddev; // m/s
  float xs3_speed_stddev; // m/s
  uint8_t validity; // 1 = valid, 0 = invalid
  float latency; // seconds
  uint16_t checksum; // unsigned sum
} __attribute__((packed));

// DVL speed compensation is simply Cext / Cdvl * Vdvl
// It can be disabled by setting to NaN 0x7FC0000

// structs are the same
typedef ground_speed water_speed;

// ////////////////////////////////////////////////////////////////////////// //
// Depth
struct depth {
  char byte;
  uint8_t size;
  uint8_t id;
  uint8_t version;
  uint32_t time_tag; // 100us
  float depth; // meters
  float depth_stddev; // meters
  uint8_t validity; // 1 = valid, 0 = invalid
  float latency; // seconds
  uint16_t checksum; // unsigned sum
} __attribute__((packed));

// ////////////////////////////////////////////////////////////////////////// //
// USBL
struct usbl {
  char byte;
  uint8_t size;
  uint8_t id;
  uint8_t version;
  uint32_t time_tag; // 100us
  char beacon_id[6];
  int32_t latitude; // +/- 2^31 = +/- pi radians
  int32_t longitude; // +/- 2^31 = +/- pi radians
  float latency; // seconds
  float altitude; // meters
  float latitude_stddev; // meters
  float longitude_stddev; // meters
  float lat_lon_covar; // meters
  float altitude_stddev; // meters
  uint8_t validity; // 1 = valid, 0 = invalid
  uint16_t checksum; // unsigned sum
} __attribute__((packed));

// ////////////////////////////////////////////////////////////////////////// //
// Defines

// General options
const static char header = '$';
const static uint8_t data_bloc_version = 0x01;
const static uint8_t valid = 1;
const static uint8_t invalid = 0;

// Telegram sizes
const static uint8_t telegram_size_gps = 48;
const static uint8_t telegram_size_usbl = 49;
const static uint8_t telegram_size_depth = 23;
const static uint8_t telegram_size_groundspeed = 51;
const static uint8_t telegram_size_waterspeed = 51;

// Telegram IDs
const static uint8_t telegram_id_gps_1 = 1;
const static uint8_t telegram_id_gps_2 = 2;
const static uint8_t telegram_id_gps_manual = 3;
const static uint8_t telegram_id_usbl = 5;
const static uint8_t telegram_id_depth = 6;
const static uint8_t telegram_id_groundspeed = 8;
const static uint8_t telegram_id_waterspeed = 9;

// GPS fields
const static uint8_t gps_quality_invalid = 0;
const static uint8_t gps_quality_natural = 1;
const static uint8_t gps_quality_differential = 2;
const static uint8_t gps_quality_military = 3;
const static uint8_t gps_quality_rtk = 4;
const static uint8_t gps_quality_float_rtk = 5;

// From the manual; not really used, but whatever
const static float gps_quality_invalid_stddev = 0.0;
const static float gps_quality_natural_stddev = 10.0;
const static float gps_quality_differential_stddev = 3.0;
const static float gps_quality_military_stddev = 10.0;
const static float gps_quality_rtk_stddev = 0.30;
const static float gps_quality_float_rtk_stddev = 10.0;

// DVL fields


uint16_t compute_checksum(uint8_t* buf, size_t msgsize) {
  uint16_t checksum = 0;
  for (size_t i=0; i<msgsize-2; i++) {
    checksum += buf[i];
  }
  return checksum;
}

} // namespace ixsea_auv

} //namespace phinsins

#endif //PHINSINS_PHINS_AUV_STRUCTS_H_
