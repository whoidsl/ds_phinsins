/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/1/20.
//
#ifndef PHINSINS_DVL_PARAMS_H
#define PHINSINS_DVL_PARAMS_H

#include <string>
#include <limits>

namespace ds_sensors {

class DvlParams {
 public:

  uint8_t dvl_id;
  int min_beams;
  double xs1_stddev;
  double xs2_stddev;
  double xs3_stddev;
  // SS - these are parameters to deal with potential different sensor frames definitions
  //      by different dvl manufacturers
  bool swap_xy; // If true, swap x and y axes of dvl instrument frame
  double xs1_scale_factor; // these are general scale factors, but mostly used
  double xs2_scale_factor; // to invert sign of an axis: set to +1 to maintain sign of
  double xs3_scale_factor; // an axis, and set to -1 to invert sign of an axis

  DvlParams() {
    dvl_id = -1;

    min_beams = 3;

    xs1_stddev = std::numeric_limits<double>::quiet_NaN();
    xs2_stddev = std::numeric_limits<double>::quiet_NaN();
    xs3_stddev = std::numeric_limits<double>::quiet_NaN();
  }

  void setupParameters(const std::string& ns) {
    min_beams = ros::param::param<int>(ns + "/min_beams", min_beams);
    xs1_stddev = ros::param::param<double>(ns + "/xs1_stddev", xs1_stddev);
    xs2_stddev = ros::param::param<double>(ns + "/xs2_stddev", xs2_stddev);
    xs3_stddev = ros::param::param<double>(ns + "/xs3_stddev", xs3_stddev);
    // SS - these are parameters to deal with potential different sensor frames definitions
    //      by different dvl manufacturers
    // JISV - as long as the coordinate frame is left-handed, it should be possible to fix this
    //      using a rotation offset configured on the PHINS
    swap_xy = ros::param::param<bool>(ns + "/swap_xy", false);
    xs1_scale_factor = ros::param::param<double>(ns + "/xs1_scale_factor", 1.0);
    xs2_scale_factor = ros::param::param<double>(ns + "/xs2_scale_factor", 1.0);
    xs3_scale_factor = ros::param::param<double>(ns + "/xs3_scale_factor", 1.0);
  }
};

} // namespace ds_sensors

#endif //PHINSINS_DVL_PARAMS_H
