/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/1/20.
//

#ifndef PHINSINS_PHINSINS_H
#define PHINSINS_PHINSINS_H

#include <ds_sensors/phinsbin.h>
#include <sensor_msgs/NavSatFix.h>
#include <ds_sensor_msgs/Dvl.h>
#include <ds_sensor_msgs/DepthPressure.h>
#include <ds_sensor_msgs/SoundSpeed.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <ds_sensor_msgs/PhinsConfig.h>
#include <dynamic_reconfigure/server.h>
#include <ds_core_msgs/VoidCmd.h>
#include <ds_core_msgs/StringCmd.h>
#include <ds_nmea_msgs/Gga.h>

#include "ds_nmea_parsers/PixseAccest.h"
#include "ds_nmea_parsers/PixseFogest.h"

#include "gps_params.h"
#include "dvl_params.h"
#include "depth_params.h"
#include "usbl_params.h"

namespace ds_sensors {

class PhinsIns : public ds_sensors::PhinsBin {
 public:
  PhinsIns();
  PhinsIns(int argc, char* argv[], const std::string& name);
  ~PhinsIns() override;
  DS_DISABLE_COPY(PhinsIns);

  void setupParameters() override;
  void setupPublishers() override;
  void setupServices() override;
  void setupSubscriptions() override;
  void setupConnections() override;
  void setupTimers() override;

 protected:
  static std::string gpsToExtbin(const sensor_msgs::NavSatFix& fix, const GpsParams& params);
  static std::string usblToExtbin(const sensor_msgs::NavSatFix& fix, const UsblParams& params);
  static std::string dvlToExtbin(const ds_sensor_msgs::Dvl& dvl, const DvlParams& params);
  static std::string depthToExtbin(const ds_sensor_msgs::DepthPressure& dep, const DepthParams& params);
  static std::string soundspeedToExtbin(const ds_sensor_msgs::SoundSpeed& sspeed, double sv_min, double sv_max);
  static std::string dvlWtToExtbin(const ds_sensor_msgs::Dvl& dvl, const DvlParams& params);
  void onGps1(const sensor_msgs::NavSatFix& gps);
  void onGps2(const sensor_msgs::NavSatFix& gps);
  void onDvl1(const ds_sensor_msgs::Dvl& dvl);
  void onDvl2(const ds_sensor_msgs::Dvl& dvl);
  void onDvlWt1(const ds_sensor_msgs::Dvl& dvl);
  void onDvlWt2(const ds_sensor_msgs::Dvl& dvl);
  void onDepth(const ds_sensor_msgs::DepthPressure& dep);
  void onUsbl(const sensor_msgs::NavSatFix& usbl);
  void onSoundspeed(const ds_sensor_msgs::SoundSpeed& sspeed);
  void onExtGps(const ds_nmea_msgs::Gga& ext_gga);

  void parseReceivedRepeater(const ds_core_msgs::RawData& bytes);
  void sendNextCommand(const ros::TimerEvent& evt);
  void dynamicParamCallback(ds_sensor_msgs::PhinsConfig &config, uint32_t level);
  bool rebootCallback(ds_core_msgs::VoidCmd::Request &req, ds_core_msgs::VoidCmd::Response& resp);

  bool reinitCallback(ds_core_msgs::StringCmd::Request &req, ds_core_msgs::StringCmd::Response& resp);
  bool usblModeCallback(ds_core_msgs::StringCmd::Request &req, ds_core_msgs::StringCmd::Response& resp);
  bool manposCallback(ds_core_msgs::VoidCmd::Request &req, ds_core_msgs::VoidCmd::Response& resp);

  // convienent handle for sending data to phins via the primary port
  boost::shared_ptr<ds_asio::DsConnection> instrument;
  boost::shared_ptr<ds_asio::DsConnection> repeater;

  std::vector<std::string> queries_;
  size_t query_idx_;
  ros::Timer query_timer_;

  ds_sensor_msgs::PhinsConfig dynamic_config_;
  ds_sensor_msgs::PhinsConfig initial_dynamic_config_;
  boost::shared_ptr<dynamic_reconfigure::Server<ds_sensor_msgs::PhinsConfig>> dr_srv_;
  boost::recursive_mutex dr_mutex_;
  bool send_on_next_config_;

  GpsParams gps1_params_;
  GpsParams gps2_params_;
  DvlParams dvl1_params_;
  DvlParams dvl2_params_;
  DvlParams dvlWt1_params_;
  DvlParams dvlWt2_params_;
  DepthParams depth_params_;
  UsblParams usbl_params_;

  ros::Subscriber sub_gps1_;
  ros::Subscriber sub_gps2_;
  ros::Subscriber sub_dvl1_;
  ros::Subscriber sub_dvl2_;
  ros::Subscriber sub_dvlWt1_;
  ros::Subscriber sub_dvlWt2_;
  ros::Subscriber sub_depth_;
  ros::Subscriber sub_usbl_;
  ros::Subscriber sub_soundspeed_;
  ros::Subscriber sub_ext_gps_;

  ros::Publisher fogest_pub_;
  ros::Publisher accest_pub_;

  ros::ServiceServer restart_srv_;
  bool restart_in_progress_;

  ros::ServiceServer reinit_srv_;

  ros::ServiceServer phins_usbl_mode_srv_;
  
  ros::ServiceServer manpos_srv_;
  
  float declat, declon, antenna_alt;

  double sv_min, sv_max;

  void sendExtSensorMessage(const std::string& msg);
  std::string appendChecksum(const std::string& base);
  void parseConfig(const std::string& message, size_t comma_idx);
  void updateInt(int& result, const std::string& field, size_t comma_idx);
  void sendCommandInt(const std::string& key, int value);
  void sendCommandTwoInts(const std::string& key, int v1, int v2);

  // Cached last positions for reinitializing ins underwater
  // after a power cycle in absence of position input
  sensor_msgs::NavSatFix last_gps1_;
  sensor_msgs::NavSatFix last_gps2_;
  sensor_msgs::NavSatFix last_usbl_;
  sensor_msgs::NavSatFix last_manual_pos_;
  
};

} // namespace ds_sensors

#endif //PHINSINS_PHINSINS_H
